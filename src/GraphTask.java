import java.util.*;

public class GraphTask {

	/**
	 * Koostada meetod, mis teeb kindlaks, kas etteantud sidusas lihtgraafis
	 * leidub Euleri ts�kkel, ning selle leidumisel nummerdab k�ik servad
	 * vastavalt nende j�rjekorrale Euleri ts�kli l�bimisel (vt. ka Fleury'
	 * algoritm).
	 * 
	 * Euleri graafi nurkade numereerimine Kasutan omas t66s jargmisi ideid ja
	 * motteid,
	 * 
	 * http://en.wikipedia.org/wiki/Eulerian_path
	 * http://www.graph-magics.com/articles/euler.php
	 * http://code.google.com/p/jalds
	 * https://sites.google.com/site/indy256/algo/euler_cycle
	 * http://mathworld.wolfram.com/EulerianGraph.html
	 * http://stones333.blogspot.com.ee/2013/11/find-eulerian-path-in-directed-
	 * graph.html
	 * http://www.cs.rpi.edu/research/groups/pb/jgb/java/rpi/goldsd/graph/
	 * Algorithms.java http://algs4.cs.princeton.edu/41graph/
	 * http://algs4.cs.princeton.edu/41graph/EulerianCycle.java
	 * http://www.cs.rpi.edu/research/groups/pb/jgb/java/rpi/goldsd/graph/Path.
	 * java
	 * https://www.cs.virginia.edu/~evans/cs201j/lectures/graph/impl2/Graph.java
	 * http://www.cs.rpi.edu/research/groups/pb/jgb/java/rpi/goldsd/graph/
	 * Algorithms.java
	 */

	public void run() {
//		Graph g = new Graph("G");
//		g.createRandomSimpleGraph(6, 9);
//		System.out.println(g);
//	}	
		Graph g = new Graph("Test 4 - Viis tippu");
		Vertex a = new Vertex("A");
		Vertex b = new Vertex("B");
		Vertex c = new Vertex("C");
		Vertex d = new Vertex("D");
		Vertex e = new Vertex("E");
		g.first = a;
		a.next = b;
		b.next = c;
		c.next = d;
		d.next = e;
		Arc ab = new Arc("AB");
		Arc bc = new Arc("BC");
		Arc cd = new Arc("CD");
		Arc de = new Arc("DE");
		Arc ea = new Arc("EA");
		Arc ad = new Arc("AD");
		a.first = ab;
		b.first = bc;
		c.first = cd;
		d.first = de;
		e.first = ea;
		ab.next = ad;
		ab.target = b;
		bc.target = c;
		cd.target = d;
		de.target = e;
		ea.target = a;
		ad.target = d;
		enumerate(g);
		System.out.println(g);
	}
	public static void main(String[] args) {
		GraphTask a = new GraphTask();
		a.run();
	}

	class Vertex {

		private String id;
		private Vertex next;
		private Arc first;
		private int info = 0;

		Vertex(String s, Vertex v, Arc e) {
			id = s;
			next = v;
			first = e;
		}

		Vertex(String s) {
			this(s, null, null);
		}

		/**
		 * http://www.oracle.com/technetwork/articles/java/index-137868.html An
		 * overriding method.
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object o) {
			if (o instanceof Vertex)
				return id.equals(((Vertex) o).id);
			return false;
		}

		/**
		 * An overriding method.
		 * 
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			return id.hashCode();
		}

		/**
		 * An overriding method.
		 * 
		 * @see java.lang.Object#toString()
		 */

		@Override
		public String toString() {
			return id;
		}

	}

	class Arc {

		private String id;
		private Vertex target;
		private Arc next;
		private int info = 0;

		Arc(String s, Vertex v, Arc a) {
			id = s;
			target = v;
			next = a;
		}

		Arc(String s) {
			this(s, null, null);
		}

		/**
		 * An overriding method.
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object o) {
			if (o instanceof Arc)
				return id.equals(((Arc) o).id);
			return false;
		}

		/**
		 * An overriding method.
		 * 
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			return id.hashCode();
		}

		/**
		 * An overriding method.
		 * 
		 * @see java.lang.Object#toString()
		 */

		@Override
		public String toString() {
			return id;
		}

	}

	class Graph {

		private String id;
		private Vertex first;
		private int info = 0;

		Graph(String s, Vertex v) {
			id = s;
			first = v;
		}

		Graph(String s) {
			this(s, null);
		}

		/**
		 * Returns list of vertices.
		 * 
		 * @return list of vertices.
		 */
		public ArrayList<Vertex> vertices() {
			ArrayList<Vertex> vertices = new ArrayList<Vertex>();
			Vertex v = first;
			while (v != null) {
				vertices.add(v);
				Arc e = v.first;
				while (e != null) {
					e.target.info++;
					v.info++;
					e = e.next;
				}
				v = v.next;
			}
			return vertices;
		}

		/**
		 * Returns the number of edges.
		 * 
		 * @return the number of edges.
		 */
		public int edges() {
			int result = 0;
			Vertex v = first;
			while (v != null) {
				Arc a = v.first;
				while (a != null) {
					result++;
					a = a.next;
				}
				v = v.next;
			}
			return result;
		}

		/**
		 * Returns the graph image.
		 * 
		 * @return the graph image.
		 */
		@Override
		public String toString() {
			String nl = System.getProperty("line.separator");
			StringBuffer sb = new StringBuffer(nl);
			sb.append(id);
			sb.append(nl);
			Vertex v = first;
			while (v != null) {
				sb.append(v.toString() + "(" + v.info + ")");
				sb.append(" -->");
				Arc a = v.first;
				while (a != null) {
					sb.append(" ");
					sb.append(a.toString());
					sb.append(" (");
					sb.append(v.toString());
					sb.append("->");
					sb.append(a.target.toString() + ", " + a.info);
					sb.append(")");
					a = a.next;
				}
				sb.append(nl);
				v = v.next;
			}
			return sb.toString();
		}

		public Vertex createVertex(String vid) {
			Vertex res = new Vertex(vid);
			res.next = first;
			first = res;
			return res;
		}

		public Arc createArc(String aid, Vertex from, Vertex to) {
			Arc res = new Arc(aid);
			res.next = from.first;
			from.first = res;
			res.target = to;
			return res;
		}

		/**
		 * Create a connected undirected random tree with n vertices. Each new
		 * vertex is connected to some random existing vertex.
		 * 
		 * @param n
		 *            number of vertices added to this graph
		 */
		public void createRandomTree(int n) {
			if (n <= 0)
				return;
			Vertex[] varray = new Vertex[n];
			for (int i = 0; i < n; i++) {
				varray[i] = createVertex("v" + String.valueOf(n - i));
				if (i > 0) {
					int vnr = (int) (Math.random() * i);
					createArc("a" + varray[vnr].toString() + "_" + varray[i].toString(), varray[vnr], varray[i]);
					createArc("a" + varray[i].toString() + "_" + varray[vnr].toString(), varray[i], varray[vnr]);
				} else {
				}
			}
		}

		/**
		 * Create an adjacency matrix of this graph. Side effect: corrupts info
		 * fields in the graph
		 * 
		 * @return adjacency matrix
		 */
		public int[][] createAdjMatrix() {
			info = 0;
			Vertex v = first;
			while (v != null) {
				v.info = info++;
				v = v.next;
			}
			int[][] res = new int[info][info];
			v = first;
			while (v != null) {
				int i = v.info;
				Arc a = v.first;
				while (a != null) {
					int j = a.target.info;
					res[i][j]++;
					a = a.next;
				}
				v = v.next;
			}
			return res;
		}

		/**
		 * Create a connected simple (undirected, no loops, no multiple arcs)
		 * random graph with n vertices and m edges.
		 * 
		 * @param n
		 *            number of vertices
		 * @param m
		 *            number of edges
		 */
		public void createRandomSimpleGraph(int n, int m) {
			if (n <= 0)
				return;
			if (n > 2500)
				throw new IllegalArgumentException("Too many vertices: " + n);
			if (m < n - 1 || m > n * (n - 1) / 2)
				throw new IllegalArgumentException("Impossible number of edges: " + m);
			first = null;
			createRandomTree(n); // n-1 edges created here
			Vertex[] vert = new Vertex[n];
			Vertex v = first;
			int c = 0;
			while (v != null) {
				vert[c++] = v;
				v = v.next;
			}
			int[][] connected = createAdjMatrix();
			int edgeCount = m - n + 1; // remaining edges
			while (edgeCount > 0) {
				int i = (int) (Math.random() * n); // random source
				int j = (int) (Math.random() * n); // random target
				if (i == j)
					continue; // no loops
				if (connected[i][j] != 0 || connected[j][i] != 0)
					continue; // no multiple edges
				Vertex vi = vert[i];
				Vertex vj = vert[j];
				createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
				connected[i][j] = 1;
				createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
				connected[j][i] = 1;
				edgeCount--; // a new edge happily created
			}
		}

	}
	/**
	 * Enumerate Eulerian edges.
	 * @param g Eulerian graph.
	 * @throws RuntimeException  If an input is not Eulerian graph or has no Eulerian path.
 	 */
	public void enumerate(Graph g) {
		ArrayList<Vertex> vertices = g.vertices();
		int i = 0;
		for (Vertex vertex : vertices)
			if (vertex.info % 2 == 1)
				i++;
		if (i > 2)
			throw new RuntimeException("It is no Eulerian graph: " + g.id );
		Vertex v = g.first;
		for (i = 0; i < g.edges(); i++) {
			if (v.first.info == 0) {
				v.first.info = i + 1;
				v = v.first.target;
			} else {
				Arc e = v.first.next;
				if (e == null)
					throw new RuntimeException("It has no Eulerian path: " + g.id );
				while (e.info != 0)
					e = e.next;
				v.first.next.info = i + 1;
				v = e.target;
			}
		}
	}
	

}